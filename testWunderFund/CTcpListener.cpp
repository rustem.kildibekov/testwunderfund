#include "CTcpListener.h"
#include <iostream>
#include <future>
#include <thread>
#include <vector>

CTcpListener::CTcpListener(std::string ipAddress, int port, MessageRecievedHandler handler)
	: m_ipAddress(ipAddress), m_port(port), MessageReceived(handler)
{
}

CTcpListener::~CTcpListener()
{
	Cleanup();
}

void CTcpListener::Send(int clientSocket, std::string msg)
{
	send(clientSocket, msg.c_str(), msg.size() + 1, 0);

}

bool CTcpListener::Init()
{
	WSADATA data;
	WORD ver = MAKEWORD(2, 2);

	int wsInit = WSAStartup(ver, &data);
	

	return wsInit == 0;
}

void CTcpListener::Run()
{
	

	SOCKET listening = CreateSocket();
	if (listening == INVALID_SOCKET) {
		return;
	}

	fd_set master;
	FD_ZERO(&master);
	FD_SET(listening, &master);

	char buf[MAX_BUFFER_SIZE];

	while (true) {
		fd_set copy = master;


		int socketCount = select(0, &copy, nullptr, nullptr, nullptr);

		for (int i = 0; i < socketCount; i++) {


			SOCKET sock = copy.fd_array[i];
			if (sock == listening) {
				SOCKET client = accept(listening, nullptr, nullptr);
				FD_SET(client, &master);
				std::string welcomeMsg = "Welcome to the Chat Server!\r\n";
				MessageReceived(this, client, welcomeMsg);
			}
			else {
				
					int bytesReceived = 0;
					
					ZeroMemory(buf, MAX_BUFFER_SIZE);
					bytesReceived = recv(sock, buf, MAX_BUFFER_SIZE, 0);
					if (bytesReceived > 0)
					{
						if (MessageReceived != NULL) {
							for (int i = 0; i < master.fd_count; i+=4) {							
								auto future1 = std::async(std::launch::async, [&master, &sock, &listening, this, i, buf, bytesReceived] {
									if (i >= master.fd_count)
										return;
									SOCKET sockOther = master.fd_array[i];
									if (sockOther != listening && sockOther != sock) {
										MessageReceived(this, sockOther, std::string(buf, 0, bytesReceived));
									}
								});
								auto future2 = std::async(std::launch::async, [&master, &sock, &listening, this, i, buf, bytesReceived] {
									if (i + 1 >= master.fd_count)
										return;
									SOCKET sockOther = master.fd_array[i+1];
									if (sockOther != listening && sockOther != sock) {
										MessageReceived(this, sockOther, std::string(buf, 0, bytesReceived));
									}
								});
								auto future3 = std::async(std::launch::async, [&master, &sock, &listening, this, i, buf, bytesReceived] {
									if (i + 2  >= master.fd_count)
										return;
									SOCKET sockOther = master.fd_array[i+2];
									if (sockOther != listening && sockOther != sock) {
										MessageReceived(this, sockOther, std::string(buf, 0, bytesReceived));
									}
								});
								auto future4 = std::async(std::launch::async, [&master, &sock, &listening, this, i, buf, bytesReceived] {
									if (i + 3 >= master.fd_count)
										return;
									SOCKET sockOther = master.fd_array[i+3];
									if (sockOther != listening && sockOther != sock) {
										MessageReceived(this, sockOther, std::string(buf, 0, bytesReceived));
									}
								});
							}
						}
					}
					else {
						closesocket(sock);
						FD_CLR(sock, &master);
					}
				

			
			}

		}

	}

	fd_set copy = master;
	for (int i = copy.fd_count - 1; i >= 0; i--) {
		SOCKET sock = master.fd_array[i];
		closesocket(listening);
	}
	
}

void CTcpListener::Cleanup()
{
	WSACleanup();
}

SOCKET CTcpListener::CreateSocket()
{
	SOCKET listening = socket(AF_INET, SOCK_STREAM, 0);

	if (listening != INVALID_SOCKET) {
		sockaddr_in hint;
		hint.sin_family = AF_INET;
		hint.sin_port = htons(m_port);
		inet_pton(AF_INET, m_ipAddress.c_str(), &hint.sin_addr);

		int bindOk = bind(listening, (sockaddr*)&hint, sizeof(hint));
		if (bindOk != SOCKET_ERROR) {
			int listenOk = listen(listening, SOMAXCONN);
			if (listenOk == SOCKET_ERROR) {
				return -1;
			}
		}
		else {
			return -1;
		}
	}

	return listening;
}
SOCKET CTcpListener::WaitForConnection(SOCKET listening)
{
	SOCKET client = accept(listening, NULL, NULL);
	return client;
}